## Messenger App `(WIP)`

A Messenger clone using React Hooks and Firebase ( for now, considering switching to GraphQL ):

#### Instructions

1. `yarn start`
   Run this command in the root of the project to start the app
2. `yarn test`
   Run this command in the root of the project to run unit tests
3. `yarn build`
   Run this command to build the app ready for production. It build to a 'build' folder

#### App improvements to implement

- [x] ~~To write Unit tests~~
- [x] ~~Make it possible to select a user and create a new message for her~~
- [x] ~~Indicate new messages~~
- [ ] Declarative vs Imperative
- [x] ~~Sort messages correctly (add createdAt property)~~
- [x] ~~Add types checking~~
- [ ] Implement `View more` to load not displayed messages
- [ ] Make app more accessible
- [ ] Switch to GraphQL
- [ ] Cache fetching results (intelocutors)
- [ ] Make the app responsive
- [ ] Add service workers to cache data so the app is user friendly when offline

#### Comments about the app
* Unread user messages are indicated in the app. To change message read status user will focus on the new message input field.
* Some of the context values in let's say MessageInput are used only for passing to api requests, therefore get rerendered every time the context value change. With redux and redux-saga this login will stay in the saga itself, which provides a nice way to decouple logic and avoid unnecessary rerenders

## Learn More

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment
