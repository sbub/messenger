//@flow
import React, { createContext, useReducer, useContext } from "react";
import interlocutorReducer, { initialState } from "./reducer";

import type { InterlocutorProps } from "types/interlocutor";
import type { ChildProps } from "types/child";

const InterlocutorDispatch: Object = createContext();
const InterlocutorState: Object = createContext();

// making provider available
const InterlocutorProvider = ({ children }: ChildProps) => {
  const [state, dispatch] = useReducer(interlocutorReducer, initialState);

  return (
    <InterlocutorDispatch.Provider value={dispatch}>
      <InterlocutorState.Provider value={state} children={children} />
    </InterlocutorDispatch.Provider>
  );
};

export function useInterlocutorDispatch() {
  return useContext(InterlocutorDispatch);
}

export function useInterlocutorState(): InterlocutorProps {
  return useContext(InterlocutorState);
}

export default InterlocutorProvider;
