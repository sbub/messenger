//@flow

import { SET_INTERLOCUTOR } from "./action-types";

import type { InterlocutorProps } from "types/interlocutor";

type StateProps = InterlocutorProps | null;

const initialState = null;

const reducer = (state: StateProps, action: Object): StateProps => {
  switch (action.type) {
    case SET_INTERLOCUTOR:
      return action.payload;
    default:
      return state;
  }
};

export { initialState, reducer as default };
