//@flow
import React, { createContext, useReducer, useContext } from "react";
import authReducer, { initialState } from "./reducer";

import type { ChildProps } from "types/child";
import type { AuthProps } from "types/auth";

const AuthDispatch: Object = createContext();
const AuthState: Object = createContext();

// making provider available
export function AuthProvider({ children }: ChildProps) {
  const [state, dispatch] = useReducer(authReducer, initialState);

  // using two different contexts here
  // to avoid unnecessary rerenders of components
  // that will only use dispatch fn
  // Reference: https://reactjs.org/docs/hooks-faq.html#how-to-avoid-passing-callbacks-down
  return (
    <AuthDispatch.Provider value={dispatch}>
      <AuthState.Provider value={state} children={children} />
    </AuthDispatch.Provider>
  );
}

export function useAuthDispatch() {
  return useContext(AuthDispatch);
}

export function useAuthState(): AuthProps {
  return useContext(AuthState);
}
