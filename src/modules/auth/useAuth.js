import { useEffect } from "react";
import { AUTH_CHANGE } from "./action-types";
import { useAuthState, useAuthDispatch } from "./context";
import { onAuthStateChanged } from "utils/firebase";

export default function useAuth() {
  const { authAttempted, auth } = useAuthState();
  const dispatch = useAuthDispatch();

  useEffect(() => {
    return onAuthStateChanged(auth => {
      if (auth) {
        const { displayName, photoURL, uid } = auth;
        dispatch({
          type: AUTH_CHANGE,
          auth: { displayName, photoUrl: photoURL, uid }
        });
      } else {
        dispatch({ type: AUTH_CHANGE, auth: null });
      }
    });
  }, [dispatch]);

  return { authAttempted, auth };
}
