import { AUTH_CHANGE, UPDATE_USER } from "./action-types";

const initialState = {
  authAttempted: false,
  auth: null
};

const authReducer = (state, action) => {
  switch (action.type) {
    case AUTH_CHANGE: {
      return {
        ...state,
        auth: action.auth,
        authAttempted: true
      };
    }
    case UPDATE_USER: {
      return {
        ...state,
        auth: {
          ...state.auth,
          displayName: action.payload.displayName,
          photoUrl: action.payload.photoUrl
        }
      };
    }
    default:
      return state;
  }
};

export { initialState, authReducer as default };
