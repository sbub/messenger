//@flow
import React, { createContext, useReducer, useContext } from "react";
import messagesReducer, { initialState } from "./reducer";

import type { MessageProps } from "types/message";
import type { ChildProps } from "types/message";

type StateProps = {
  selectedMessage: MessageProps | null,
  messages: Array<MessageProps>
};

const MessagesState: Object = createContext();
const MessagesDispatch: Object = createContext();

const MessagesProvider = ({ children }: ChildProps) => {
  const [state, dispatch] = useReducer(messagesReducer, initialState);
  return (
    <MessagesDispatch.Provider value={dispatch}>
      <MessagesState.Provider value={state} children={children} />
    </MessagesDispatch.Provider>
  );
};

export const useMessagesState = (): StateProps => {
  return useContext(MessagesState);
};
export const useMessagesDispatch = () => {
  return useContext(MessagesDispatch);
};

export default MessagesProvider;
