//@flow
import { SET_MESSAGES, SELECT_MESSAGE } from "./action-types";

import type { MessageProps } from "types/message";

type StateProps = {
  selectedMessage: MessageProps | null,
  messages: Array<MessageProps>
};

const initialState = {
  selectedMessage: null,
  messages: []
};

const messagesReducer = (state: StateProps, action: Object): StateProps => {
  switch (action.type) {
    case SET_MESSAGES: {
      return onSetMessages(state, action);
    }
    case SELECT_MESSAGE: {
      return onSelectMessage(state, action);
    }
    default:
      return state;
  }
};

const onSetMessages = (state, action) => {
  return {
    ...state,
    selectedMessage: state.selectedMessage
      ? state.selectedMessage
      : action.messages[0],
    messages: action.messages
  };
};

const onSelectMessage = (state, action) => {
  // [scenario]: create new message. if there was communication with this user then select this message and thread
  // passing uid on action object to find if messages
  // with selected user already exist
  if (action.uid) {
    const [userMessages] = state.messages.filter(message => {
      return message.uids.indexOf(action.uid) > -1;
    });
    if (userMessages) {
      return {
        ...state,
        selectedMessage: userMessages
      };
    } else {
      return state;
    }
  }
  return {
    ...state,
    selectedMessage: action.payload
  };
};

export { initialState, messagesReducer as default };
