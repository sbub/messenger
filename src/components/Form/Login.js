//@flow
import React, { useState } from "react";
import styles from "./Login.module.css";

import useForm from "./useForm";
import validateLogin from "./validateLogin";
import { login, signup } from "utils/firebase";
import { UPDATE_USER } from "modules/auth/action-types";
import { useAuthDispatch } from "modules/auth/context";

import Spinner from "components/Spinner";

const initialState = {
  displayName: "",
  photoUrl: "",
  email: "",
  password: ""
};

const InlineError = ({ text }: { text: string }) => {
  return (
    <p data-testid="field-error" className={styles.errorMessage}>
      {text}
    </p>
  );
};

const Login = () => {
  const [isLogin, setIsLogin] = useState(true);
  const [firebaseError, setFirebaseError] = React.useState(null);
  const dispatch = useAuthDispatch();
  const { values, handleChange, submitting, handleSubmit, errors } = useForm(
    initialState,
    validateLogin,
    authenticateUser
  );
  async function authenticateUser() {
    try {
      const { displayName, photoUrl, email, password } = values;
      isLogin
        ? await login(email, password)
        : await signup({ displayName, photoUrl, email, password });
      !isLogin &&
        dispatch({ type: UPDATE_USER, payload: { displayName, photoUrl } });
    } catch (err) {
      setFirebaseError(err.message);
    }
  }
  const handleKeyPress = e => {
    if (e.charCode === 13) {
      setIsLogin(prevState => !prevState);
    }
  };
  return (
    <form onSubmit={handleSubmit} className={styles.form}>
      {!isLogin && (
        <input
          type="text"
          name="displayName"
          placeholder="Displayed name"
          value={values.displayName}
          onChange={handleChange}
          className={styles.field}
        />
      )}
      {!isLogin && (
        <input
          type="text"
          name="photoUrl"
          placeholder="Avatar url"
          value={values.photoUrl}
          onChange={handleChange}
          className={styles.field}
        />
      )}
      <input
        type="email"
        name="email"
        placeholder="Email"
        value={values.email}
        onChange={handleChange}
        className={styles.field}
      />
      {errors.email && <InlineError text={errors.email} />}
      <input
        type="password"
        name="password"
        placeholder="Password"
        value={values.password}
        onChange={handleChange}
        className={styles.field}
      />
      {errors.password && <InlineError text={errors.password} />}
      {firebaseError && <InlineError text={firebaseError} />}
      <div className={styles.btnGroup}>
        {submitting ? (
          <Spinner />
        ) : (
          <button
            type="submit"
            disabled={submitting}
            className={`btn ${Object.keys(errors).length && styles.disabled}`}
          >
            Submit
          </button>
        )}

        <button
          type="button"
          disabled={submitting}
          onClick={() => setIsLogin(prevState => !prevState)}
          onKeyPress={handleKeyPress}
          className={`btn ${styles.secondary}`}
          data-testid="alternative-authentication"
        >
          {isLogin
            ? "Don't have an account yet ?"
            : "Already have an account ?"}
        </button>
      </div>
    </form>
  );
};

export default Login;
