import { useEffect, useState } from "react";

const useForm = (initialState, validate, authenticate) => {
  const [values, setValues] = useState(initialState);
  const [errors, setErrors] = useState({});
  const [submitting, setSubmitting] = useState(false);

  useEffect(() => {
    let isCurrent = true;
    if (isCurrent) {
      (async () => {
        try {
          const valid = !Object.keys(errors).length;
          if (valid && submitting) {
            await authenticate();
          }
          isCurrent && setSubmitting(false);
        } catch (err) {
          console.error("Error authenticating a user");
        }
      })();
    }
    return () => {
      isCurrent = false;
    };
  }, [errors, authenticate, submitting]);

  const handleChange = e => {
    e.persist();
    setValues(prevValues => ({
      ...prevValues,
      [e.target.name]: e.target.value
    }));
  };

  const handleSubmit = e => {
    e.preventDefault();
    // 🧠 if both 'submitting' is true and form is valid => !Object.keys(errors).length
    // authentication will be invoked automatically
    setSubmitting(true);
    const validationErrors = validate(values);
    setErrors(validationErrors);
  };

  return { handleChange, handleSubmit, values, errors, submitting };
};

export default useForm;
