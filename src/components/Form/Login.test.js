import React from "react";
import { render, fireEvent, wait } from "@testing-library/react";

import Login from "./Login";

describe("displays Login component", () => {
  test("with correct fields", () => {
    const {
      getByPlaceholderText,
      queryByPlaceholderText,
      queryByTestId
    } = render(<Login />);
    expect(queryByPlaceholderText("Displayed name")).toBeNull();
    expect(queryByPlaceholderText("Avatar url")).toBeNull();
    expect(getByPlaceholderText("Email")).toBeInTheDocument();
    expect(getByPlaceholderText("Password")).toBeInTheDocument();

    expect(queryByTestId("alternative-authentication").textContent).toContain(
      "Don't have an account yet"
    );

    // Bringng up register form
    // console.log("💔", container.querySelectorAll("button"));
    fireEvent.click(queryByTestId("alternative-authentication"));
    expect(getByPlaceholderText("Displayed name")).toBeInTheDocument();
    expect(getByPlaceholderText("Avatar url")).toBeInTheDocument();
    expect(getByPlaceholderText("Email")).toBeInTheDocument();
    expect(getByPlaceholderText("Password")).toBeInTheDocument();
  });

  test("displays correct input values and error messages", async () => {
    const {
      getByPlaceholderText,
      getByText,
      getByTestId,
      queryByTestId
    } = render(<Login />);
    const email = "user@example.com";

    // fill in email input
    fireEvent.change(getByPlaceholderText("Email"), {
      target: { value: email }
    });

    expect(getByPlaceholderText("Email").value).toBe(email);

    // after submit we will see an inline error for the password field
    fireEvent.click(getByText("Submit"));
    await wait();

    expect(getByTestId("field-error")).toBeInTheDocument();
    expect(getByTestId("field-error").textContent).toBe("Password is required");

    fireEvent.change(getByPlaceholderText("Password"), {
      target: { value: 12345678 }
    });
    fireEvent.click(getByText("Submit"));
    await wait();

    expect(queryByTestId("field-error")).toBeNull();
  });
});
