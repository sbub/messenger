import React from "react";
import { render } from "@testing-library/react";

import User from "./index";

test("displays the User with a corrrect name and photoUrl", () => {
  const url =
    "https://i.pinimg.com/564x/dd/ca/66/ddca6697f45a9ee3d3f56ec917d2e240.jpg";
  const name = "Sveta Buben";
  const { container, getByText } = render(
    <User photoUrl={url} displayName={name} />
  );
  const avatar = container.querySelector("img");
  expect(getByText(name)).toBeInTheDocument();
  expect(avatar.getAttribute("src")).toBe(url);
});
