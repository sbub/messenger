//@flow
import React from "react";
import styles from "./User.module.css";
import Avatar from "components/Avatar";

const User = ({
  photoUrl,
  displayName
}: {
  photoUrl: string,
  displayName: string
}) => {
  return (
    <div className={styles.container}>
      <Avatar src={photoUrl} />
      <p className={styles.name}>{displayName}</p>
    </div>
  );
};

export default User;
