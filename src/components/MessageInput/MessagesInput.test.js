import React from "react";
import { render } from "@testing-library/react";

import { useInterlocutorState } from "modules/interlocutor/context";
import { useMessagesState } from "modules/messages/context";
import { useAuthState } from "modules/auth/context";

import MessageInput from "./index";

jest.mock("modules/interlocutor/context");
jest.mock("modules/messages/context");
jest.mock("modules/auth/context");

beforeEach(() => {
  useAuthState.mockReturnValue({ auth: { uid: "abc" } });
  useMessagesState.mockReturnValue({
    threadId: "threadId",
    selectedMessageId: "messageId"
  });
});

test("displays input without Send button when iterlocutor is not define", () => {
  useInterlocutorState.mockReturnValue(null);
  const button = document.createElement("button");
  const { container } = render(<MessageInput />);
  expect(container).not.toContain(button);
});

describe("displays Send button", () => {
  beforeEach(() => {
    useInterlocutorState.mockReturnValue({ uid: "useLoadInterlocutorid" });
  });

  test("displays input with Send button when iterlocutor is defined", () => {
    const { getByText } = render(<MessageInput />);
    expect(getByText(/^Send/)).toBeInTheDocument();
  });
});
