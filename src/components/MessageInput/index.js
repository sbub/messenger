import React, { useState } from "react";
import firebase from "firebase";

import styles from "./MessageInput.module.css";
import { db } from "utils/firebase/firebase";
import { useMessagesState } from "modules/messages/context";
import { useAuthState } from "modules/auth/context";
import { useInterlocutorState } from "modules/interlocutor/context";

const messagesShortRef = db.collection("messages_short");
const threadRef = db.collection("thread");

const MessageInput = () => {
  const [message, setMessage] = useState("");
  const { selectedMessage } = useMessagesState();
  const interlocutor = useInterlocutorState();
  const { auth } = useAuthState();

  async function sendMessage() {
    try {
      // if currently authenticated user has
      // no messages at all yet
      if (!selectedMessage) {
        // 🧠first create a thread and return an id
        // of the newly created thread
        const thread = await threadRef.add({
          conversation: [
            {
              uid: auth.uid,
              text: message
            }
          ]
        });
        messagesShortRef.add({
          uids: [interlocutor.uid, auth.uid],
          last_message: { uid: auth.uid, message },
          thread_id: thread.id,
          is_read: {
            [interlocutor.uid]: false,
            [auth.uid]: true
          },
          createdAt: Date.now()
        });
      } else {
        // 🧠 update the thread and
        // last message in messages_short collection
        threadRef.doc(selectedMessage.thread_id).update({
          conversation: firebase.firestore.FieldValue.arrayUnion({
            uid: auth.uid,
            text: message
          })
        });
        messagesShortRef.doc(selectedMessage.id).update({
          last_message: { uid: auth.uid, message },
          is_read: {
            [interlocutor.uid]: false,
            [auth.uid]: true
          },
          createdAt: Date.now()
        });
      }
    } catch (err) {
      console.error("An error occured in MessageInput component", err);
    } finally {
      setMessage("");
    }
  }

  const handleKeyPress = e => {
    if (interlocutor && e.charCode === 13) {
      sendMessage();
    }
  };
  const handleFocus = () => {
    // here by focusing on the input field messages read status is changed to true;
    // meaning that message is not bold nor more
    if (selectedMessage) {
      messagesShortRef.doc(selectedMessage.id).update({
        is_read: {
          [interlocutor.uid]: selectedMessage.is_read[interlocutor.uid],
          [auth.uid]: true
        }
      });
    }
  };
  return (
    <div className={styles.container}>
      <input
        type="text"
        className={styles.messageInput}
        placeholder="Type a message"
        value={message}
        onChange={e => setMessage(e.target.value)}
        onKeyPress={handleKeyPress}
        onFocus={handleFocus}
      />
      {interlocutor && (
        <button
          type="button"
          disabled={!message}
          className="btn"
          onClick={sendMessage}
        >
          Send
        </button>
      )}
    </div>
  );
};

export default MessageInput;
