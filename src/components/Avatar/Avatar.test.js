import React from "react";
import { render } from "@testing-library/react";
import DefaultImage from "assets/Avatar.png";

import Avatar from "./index";

test("displays default image in Avatar", () => {
  const url =
    "https://i.pinimg.com/564x/cc/fe/8a/ccfe8a6e88859afae0739cb7dceed499.jpg";
  const { getByTestId, rerender } = render(<Avatar />);
  expect(getByTestId("avatar").getAttribute("src")).toEqual(DefaultImage);

  rerender(<Avatar src={url} />);
  expect(getByTestId("avatar").getAttribute("src")).toEqual(url);
});
