//@flow
import React from "react";
import styles from "./Avatar.module.css";
import Default from "assets/Avatar.png";

type Props = {
  src?: string
};

const Avatar = ({ src }: Props) => (
  <img
    data-testid="avatar"
    className={styles.avatar}
    src={src || Default}
    alt="User avatar"
  />
);

export default Avatar;
