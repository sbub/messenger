import React from 'react';

const Search = () => {
    return (
        <input type="text" placeholder="🔎 Search a message" />
    )
}

export default Search;
