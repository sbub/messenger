import React from "react";
import { render } from "@testing-library/react";
import { useMessagesState } from "modules/messages/context";
import { useAuthState } from "modules/auth/context";
import useScroll from "./useScroll";

import Conversation, { Thread } from "./index";

jest.mock("modules/messages/context");
jest.mock("modules/auth/context");
jest.mock("./useScroll");

test("displays no an empty <div /> when there is no message selected", () => {
  useAuthState.mockReturnValue({
    auth: { uid: "abc" }
  });
  useMessagesState.mockReturnValue({
    selectedMessage: null
  });
  const { container } = render(<Conversation children="Children" />);
  const threadChildren = container.firstChild.childNodes;
  // thread container displays 2 elements
  expect(threadChildren.length).toBe(2);
  // first element of thread container is an empty div;
  expect(threadChildren[0]).toMatchInlineSnapshot(`<div />`);
});

describe("displays Thread", () => {
  beforeEach(() => {
    useMessagesState.mockReturnValue({
      selectedMessage: { thread_id: "threadId" }
    });
    useAuthState.mockReturnValue({
      auth: { uid: "abc" }
    });
  });

  test("displays a thread when threadId is available", () => {
    useScroll.mockReturnValue({
      conversationBottomRef: {
        current: {
          scrollIntoView: () => {}
        }
      }
    });
    const { container } = render(<Conversation children="Children" />);
    const threadChildren = container.firstChild.childNodes;
    expect(threadChildren[0].getAttribute("class")).toContain(
      "threadContainer"
    );
  });

  test("displays a thread with a correct messages", async () => {
    const message1 = "Hey";
    const message2 = "Hi";
    const messages = [
      { uid: "abc", text: message1 },
      { uid: "def", text: message2 }
    ];
    const { queryAllByTestId, rerender } = render(<Thread messages={[]} />);
    //thread is empty initially
    expect(queryAllByTestId("message").length).toBe(0);

    rerender(<Thread messages={messages} />);
    expect(queryAllByTestId("message").length).toBe(2);
    expect(queryAllByTestId("message")[0].childNodes[1].textContent).toBe(
      message1
    );
    expect(queryAllByTestId("message")[1].childNodes[1].textContent).toBe(
      message2
    );
  });
});
