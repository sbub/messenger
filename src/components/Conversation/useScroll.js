import { useRef, useEffect } from "react";

const useScroll = length => {
  const conversationBottomRef = useRef();

  useEffect(() => {
    conversationBottomRef.current.scrollIntoView({
      behavior: "smooth"
    });
  }, [length]);

  return { conversationBottomRef };
};

export default useScroll;
