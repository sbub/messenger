//@flow
import * as React from "react";
import styles from "./Conversation.module.css";
import { db } from "utils/firebase/firebase";
import { useMessagesState } from "modules/messages/context";
import { useInterlocutorState } from "modules/interlocutor/context";
import { useAuthState } from "modules/auth/context";
import useScroll from "./useScroll";

import type { ChildProps } from "types/child";
import type { MessageProps } from "types/message";

const threadRef = db.collection("thread");

type ThreadMessageProp = {
  uid: string,
  text: string
};

type ThreadMessagesProp = {
  messages: Array<ThreadMessageProp>
};

export const Thread = ({ messages }: ThreadMessagesProp) => {
  const { auth } = useAuthState();
  const interlocutor = useInterlocutorState();
  const { conversationBottomRef } = useScroll(messages.length);

  return (
    <div className={styles.threadContainer}>
      {messages.map((message, idx) => (
        <div
          key={`${idx}${message.text}`}
          data-testid="message"
          className={`${styles.message} ${
            message.uid === auth.uid ? styles.right : ""
          }`}
        >
          <p className={styles.messageAuthor}>
            {message.uid === auth.uid
              ? "You"
              : interlocutor
              ? interlocutor.displayName
              : ""}
          </p>
          <p className={styles.messageBody}>{message.text}</p>
        </div>
      ))}
      <div ref={conversationBottomRef} />
    </div>
  );
};

const ThreadContainer = ({ threadId }) => {
  const [thread, setThread] = React.useState([]);

  React.useEffect(() => {
    return threadRef.doc(threadId).onSnapshot(snapshot => {
      const result = snapshot.data();
      setThread(result.conversation);
    });
  }, [threadId]);

  return <Thread messages={thread} />;
};

export default function({ children }: ChildProps) {
  const { selectedMessage }: MessageProps = useMessagesState();

  return (
    <div className={styles.container}>
      {selectedMessage ? (
        <ThreadContainer threadId={selectedMessage.thread_id} />
      ) : (
        <div />
      )}
      {children}
    </div>
  );
}
