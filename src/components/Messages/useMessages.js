//@flow
import { useState, useEffect } from "react";
import { db } from "utils/firebase/firebase";
import { SET_MESSAGES } from "modules/messages/action-types";
import { useAuthState } from "modules/auth/context";
import { useMessagesDispatch } from "modules/messages/context";

const messagesRef = db.collection("messages_short");

const useMessages = (): { loading: boolean, status: string | null } => {
  const [loading, setLoading] = useState(false);
  const [status, setStatus] = useState(null);
  const { auth } = useAuthState();
  const dispatch = useMessagesDispatch();

  useEffect(() => {
    setLoading(true);
    // 🧠 select all messages in 'messages_short' collection where
    // uids container authenticated user id
    return messagesRef
      .orderBy("createdAt", "desc")
      .where("uids", "array-contains", auth.uid)
      .onSnapshot(snapshot => {
        if (snapshot.size) {
          const result = snapshot.docs.map(doc => ({
            id: doc.id,
            ...doc.data()
          }));
          dispatch({ type: SET_MESSAGES, messages: result });
          setStatus(null);
        } else {
          setStatus("Ooops...No messages for you yet");
        }
        setLoading(false);
      });
  }, [dispatch, auth.uid]);

  return { loading, status };
};

export default useMessages;
