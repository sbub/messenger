import React, { useEffect, memo } from "react";
import styles from "./Messages.module.css";

import Spinner from "components/Spinner";
import Avatar from "components/Avatar";
import Circle from "components/Circle";

import { SELECT_MESSAGE } from "modules/messages/action-types";
import { SET_INTERLOCUTOR } from "modules/interlocutor/action-types";

import type { MessageProps } from "types/message";

import {
  useMessagesState,
  useMessagesDispatch
} from "modules/messages/context";
import useLoadInterlocutor from "./useLoadInterlocutor";

import { useInterlocutorDispatch } from "modules/interlocutor/context";
import { useAuthState } from "modules/auth/context";

function messageSbstr(message: string): string {
  if (message.length > 30) {
    return message.substring(0, 30) + "...";
  }
  return message;
}

type AuthStateProps = {
  auth: {
    uid: string
  }
};

type SelectedMessageProps = {
  selectedMessage: MessageProps
};

export const Message = memo(
  ({ id, lastMessage, uids, threadId, isRead, isSelected }: MessageProps) => {
    const dispatchToMessages = useMessagesDispatch();
    const dispatchToInterlocutor = useInterlocutorDispatch();
    const { auth }: AuthStateProps = useAuthState();
    // 🧠 every time selected message change
    // fetch a user info that sent the message
    const { user, loading, interlocutorError } = useLoadInterlocutor(
      false,
      uids
    );

    // 🧠 every time selected message id changes
    // update interlocutor state;
    useEffect(() => {
      if (isSelected) {
        dispatchToInterlocutor({ type: SET_INTERLOCUTOR, payload: user });
      }
    }, [isSelected, dispatchToInterlocutor, user]);

    const selectMessage = () => {
      dispatchToMessages({
        type: SELECT_MESSAGE,
        payload: {
          id,
          uids,
          thread_id: threadId,
          last_message: lastMessage,
          is_read: isRead
        }
      });
    };

    if (interlocutorError) return <p>An error occured</p>;
    return (
      <div
        className={`${styles.message} ${isSelected ? styles.active : ""}`}
        onClick={selectMessage}
      >
        {loading ? (
          <Spinner />
        ) : (
          user && (
            <>
              <div className={styles.avatar}>
                <Avatar src={user.photoUrl} />
              </div>
              <div className={styles.messageInfo}>
                <p>{user.displayName}</p>
                <p className={styles.messageText}>
                  {auth.uid === lastMessage.uid
                    ? "You: "
                    : user.uid === lastMessage.uid
                    ? `${user.displayName}: `
                    : ""}
                  <span className={!isRead[auth.uid] ? styles.new : ""}>
                    {messageSbstr(lastMessage.message)}
                  </span>
                </p>
              </div>
              {!isRead[auth.uid] && <Circle />}
            </>
          )
        )}
      </div>
    );
  }
);

export default function(props: MessageProps) {
  const { selectedMessage }: SelectedMessageProps = useMessagesState();

  return (
    <Message
      {...props}
      isSelected={selectedMessage && props.id === selectedMessage.id}
    />
  );
}
