import React from "react";

import { useInterlocutorDispatch } from "modules/interlocutor/context";
import { useMessagesDispatch } from "modules/messages/context";

import { SET_INTERLOCUTOR } from "modules/interlocutor/action-types";
import { SELECT_MESSAGE } from "modules/messages/action-types";

const NewMessage = () => {
  const dispatchToInterlocutor = useInterlocutorDispatch();
  const dispatchToMessages = useMessagesDispatch();
  const handleAddNewMessage = () => {
    dispatchToInterlocutor({ type: SET_INTERLOCUTOR, payload: null });
    dispatchToMessages({ type: SELECT_MESSAGE, payload: null });
  };
  return (
    <button className="btn" onClick={handleAddNewMessage}>
      Create a new message
    </button>
  );
};

export default NewMessage;
