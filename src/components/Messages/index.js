//@flow
import React from "react";
import styles from "./Messages.module.css";

import Spinner from "components/Spinner";
import Message from "./Message";
import NewMessage from "./NewMessage";

import type { MessageProps } from "types/message";

import { useMessagesState } from "modules/messages/context";
import useMessages from "./useMessages";

type MessagesType = {
  messages: Array<MessageProps>
};

// 🧠 to avoid messages re-rendering when selectedMessage id changes
export const Messages = React.memo<MessagesType>(
  ({ messages }: MessagesType) => {
    const { loading, status } = useMessages();

    return (
      <div className={styles.container}>
        {messages.map(message => (
          <Message
            key={message.id}
            id={message.id}
            uids={message.uids}
            threadId={message.thread_id}
            lastMessage={message.last_message}
            isRead={message.is_read}
          />
        ))}
        {!messages.length && (
          <p data-testid="no-messages-status" className={styles.status}>
            {status}
          </p>
        )}
        {loading && <Spinner />}
      </div>
    );
  }
);

export default function() {
  const { messages }: MessagesType = useMessagesState();
  // 🧠 to avoid messages re-rendering when selectedMessage id changes
  return (
    <>
      <NewMessage />
      <Messages messages={messages} />
    </>
  );
}
