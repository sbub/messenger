//@flow
import { useEffect, useState } from "react";
import { useAuthState } from "modules/auth/context";
import { loadOneUser, loadAllUsers } from "utils/loadUsers";

type User = {
  id: string,
  displayName: string,
  photoUrl: string,
  uid: string
} | null;

type Error = string | null;

const useLoadInterlocutor = (
  isGettingAll: boolean = false,
  uids: Array<string> = []
): {
  user: User,
  interlocutorError: Error,
  loading: boolean,
  users: Array<User>
} => {
  const [user, setUser] = useState(null);

  const [interlocutorError, setInterlocutorError] = useState(null);
  const [loading, setLoading] = useState(false);

  // this is used when getting all interlocutors
  const [users, setUsers] = useState([]);

  const { auth } = useAuthState();

  useEffect(() => {
    let isCurrent = true;
    (async () => {
      try {
        let result;
        isCurrent && setLoading(true);
        if (isGettingAll) {
          result = await loadAllUsers(auth.uid);
          isCurrent && setUsers(result);
        } else {
          result = await loadOneUser(uids, auth.uid);
          isCurrent && setUser(result);
        }
        isCurrent && setLoading(false);
      } catch (err) {
        isCurrent && setInterlocutorError(err);
      }
    })();
    return () => {
      isCurrent = false;
    };
  }, []);

  return { user, interlocutorError, loading, users };
};

export default useLoadInterlocutor;
