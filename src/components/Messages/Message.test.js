import React from "react";
import { render } from "@testing-library/react";
import {
  useMessagesDispatch,
  useMessagesState
} from "modules/messages/context";
import { useInterlocutorDispatch } from "modules/interlocutor/context";
import { useAuthState } from "modules/auth/context";
import useLoadInterlocutor from "./useLoadInterlocutor";

import { Message } from "./Message";

jest.mock("modules/messages/context");
jest.mock("modules/interlocutor/context");
jest.mock("modules/auth/context");
jest.mock("./useLoadInterlocutor");

test("displays Message component with correct props", () => {
  useAuthState.mockReturnValue({
    auth: { uid: "abc" }
  });
  useInterlocutorDispatch.mockReturnValue(() => {});
  useMessagesDispatch.mockReturnValue(() => {});
  useMessagesState.mockReturnValue({
    selectedMessage: { id: "abc" }
  });
  useLoadInterlocutor.mockReturnValue({
    user: { displayName: "User5" },
    loading: false,
    interlocutorError: null
  });

  const props = {
    id: "abc",
    lastMessage: { uid: "abc", message: "Good night!" },
    uids: ["1", "2"],
    threadId: "threadId",
    isRead: {
      1: true,
      2: false
    },
    isSelected: true
  };
  const { container, queryByTestId, getByText, rerender } = render(
    <Message {...props} />
  );
  expect(queryByTestId("spinner")).toBeNull();
  expect(container.firstChild.getAttribute("class")).toContain("active");
  expect(getByText("User5")).toBeInTheDocument();

  useLoadInterlocutor.mockReturnValue({
    interlocutorError: true
  });
  rerender(<Message />);
  expect(getByText("An error occured")).toBeInTheDocument();
});
