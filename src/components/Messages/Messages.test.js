import React from "react";
import { render } from "@testing-library/react";
import useMessages from "./useMessages";
import { useMessagesState } from "modules/messages/context";
import { useAuthState } from "modules/auth/context";

import { Messages } from "./index";

jest.mock("./useMessages");
jest.mock("modules/messages/context");
jest.mock("modules/auth/context");

describe("displays Messages", () => {
  useMessages.mockReturnValue({
    loading: true
  });
  test("loading state", () => {
    const { getByTestId } = render(<Messages messages={[]} />);
    expect(getByTestId("spinner")).toBeInTheDocument();
  });
  test("no messages status", () => {
    const { getByTestId } = render(<Messages messages={[]} />);
    expect(getByTestId("no-messages-status")).toBeInTheDocument();
  });
});

test("displays corrent number of messages", () => {
  useMessages.mockReturnValue({
    loading: false
  });
  useMessagesState.mockReturnValue({
    selectedMessageId: "123"
  });
  useAuthState.mockReturnValue({
    auth: { uid: "abc" }
  });
  const messages = [
    { id: 1, uids: ["abc", "def"], threadId: "threadId", lastMessage: "Hey" }
  ];
  const { container } = render(<Messages messages={messages} />);
  expect(container.firstChild.childNodes.length).toBe(1);
});
