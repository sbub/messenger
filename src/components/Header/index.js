//@flow
import React from "react";
import styles from "./Header.module.css";
import User from "components/User";
import UserSelect from "components/UserSelect";
import { useInterlocutorState } from "modules/interlocutor/context";
import { useMessagesState } from "modules/messages/context";

import type { MessageProps } from "types/message";
import type { InterlocutorProps } from "types/interlocutor";

type SelectedMessageType = {
  selectedMessage: MessageProps
};

const Header = () => {
  const interlocutor: InterlocutorProps | null = useInterlocutorState();
  const { selectedMessage }: SelectedMessageType = useMessagesState();

  return (
    <div className={styles.container}>
      {interlocutor && selectedMessage ? (
        <User
          photoUrl={interlocutor.photoUrl}
          displayName={interlocutor.displayName}
        />
      ) : (
        <UserSelect />
      )}
    </div>
  );
};

export default Header;
