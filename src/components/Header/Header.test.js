import React from "react";
import { render } from "@testing-library/react";

import { useInterlocutorState } from "modules/interlocutor/context";
import { useAuthState } from "modules/auth/context";
import { useMessagesState } from "modules/messages/context";

import Header from "./index";

jest.mock("modules/interlocutor/context");
jest.mock("modules/auth/context");
jest.mock("modules/messages/context");

beforeEach(() => {
  useAuthState.mockReturnValue({ auth: { uid: "abc" } });
  useMessagesState.mockReturnValue({
    selectedMessage: { id: "abc" }
  });
});

test("displays UserSelect component to select interlocutor", () => {
  useInterlocutorState.mockReturnValue(null);
  const { getByText } = render(<Header />);

  expect(getByText("Click to select a user")).toBeInTheDocument();
});

test("displays User component when interlocutor is defined", () => {
  useInterlocutorState.mockReturnValue({
    displayName: "User2",
    photoUrl: "URL"
  });
  const { container } = render(<Header />);

  expect(container.querySelector("p")).toBeInTheDocument();
  expect(container.querySelector("p").textContent).toBe("User2");
});
