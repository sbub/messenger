//@flow
import React, { useState } from "react";
import styles from "./UserSelect.module.css";
import Spinner from "components/Spinner";
import User from "components/User";
import useLoadInterlocutor from "../Messages/useLoadInterlocutor";
import { useMessagesDispatch } from "modules/messages/context";
import { SET_INTERLOCUTOR } from "modules/interlocutor/action-types";
import { SELECT_MESSAGE } from "modules/messages/action-types";
import {
  useInterlocutorDispatch,
  useInterlocutorState
} from "modules/interlocutor/context";

import type { ChildProps } from "types/child";
import type { InterlocutorProps } from "types/interlocutor";

export const Results = ({ children }: ChildProps) => {
  return (
    <div data-testid="users-list" className={styles.resultsContainer}>
      {children}
    </div>
  );
};

const UserSelect = () => {
  const [showResults, setShowResults] = useState(false);
  const interlocutor = useInterlocutorState();
  const dispatchToInterlocutor = useInterlocutorDispatch();
  const dispatchToMessages = useMessagesDispatch();

  const handleUserSelect = user => {
    dispatchToInterlocutor({ type: SET_INTERLOCUTOR, payload: user });
    if (user) {
      dispatchToMessages({ type: SELECT_MESSAGE, uid: user.uid });
    }
    setShowResults(false);
  };

  const {
    loading,
    users
  }: {
    loading: boolean,
    users: Array<InterlocutorProps>
  } = useLoadInterlocutor(true);

  return (
    <div className={styles.container}>
      <p>To:</p>
      {interlocutor ? (
        <>
          <User
            photoUrl={interlocutor.photoUrl}
            displayName={interlocutor.displayName}
          />
          <button className="btn" onClick={() => handleUserSelect(null)}>
            ╳
          </button>
        </>
      ) : (
        <>
          <button
            type="button"
            className="btn"
            onClick={() => setShowResults(prevState => !prevState)}
          >
            Click to select a user
          </button>
          {showResults && (
            <Results>
              {loading && <Spinner />}
              {users.map(user => (
                <div
                  className={styles.areaSelect}
                  key={user.id}
                  onClick={() => handleUserSelect(user)}
                >
                  <User
                    photoUrl={user.photoUrl}
                    displayName={user.displayName}
                  />
                </div>
              ))}
            </Results>
          )}
        </>
      )}
    </div>
  );
};

export default UserSelect;
