import React from "react";
import { render, fireEvent } from "@testing-library/react";

import useLoadInterlocutor from "../Messages/useLoadInterlocutor";
import { useInterlocutorDispatch } from "modules/interlocutor/context";
import { useMessagesDispatch } from "modules/messages/context";

import UserSelect from "./index";

jest.mock("modules/interlocutor/context");
jest.mock("modules/messages/context");
jest.mock("../Messages/useLoadInterlocutor");

test("displays UserSelect component", () => {
  useInterlocutorDispatch.mockReturnValue(() => {});
  useMessagesDispatch.mockReturnValue(() => {});
  useLoadInterlocutor.mockReturnValue({
    loading: false,
    users: [
      { id: "1", displayName: "user1" },
      { id: "2", displayName: "user2" }
    ]
  });

  const { getByText, queryByText, queryByTestId } = render(<UserSelect />);
  expect(getByText("Click to select a user")).toBeInTheDocument();

  expect(queryByText("user1")).toBeNull();

  // open a dropdown with UserSelect
  fireEvent.click(getByText("Click to select a user"));
  expect(queryByTestId("users-list")).toBeInTheDocument();

  // selecting a user from a dropdown
  fireEvent.click(queryByTestId("users-list").childNodes[0]);
  expect(queryByTestId("users-list")).toBeNull();
});
