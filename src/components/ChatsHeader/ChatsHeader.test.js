import React from "react";
import { render } from "@testing-library/react";
import { useAuthState } from "modules/auth/context";

import ChatsHeader from "./index";

jest.mock("modules/auth/context");

test("displays ChatsHeader components with correct user info", () => {
  useAuthState.mockReturnValue({
    auth: {
      displayName: "user1"
    }
  });
  const { getByText } = render(<ChatsHeader />);
  expect(getByText("user1")).toBeInTheDocument();
});
