//@flow
import React from "react";
import styles from "./ChatsHeader.module.css";
import Avatar from "components/Avatar";
import { useAuthState } from "modules/auth/context";
import { logout } from "utils/firebase";

type authStateProps = {
  auth: {
    uid: string,
    displayName?: string,
    photoUrl?: string
  }
};

const ChatsHeader = () => {
  const { auth }: authStateProps = useAuthState();
  return (
    <div>
      <div className={styles.userPanel}>
        <h1>{auth.displayName}</h1>
        <button className="btn" type="text" onClick={logout}>
          Logout
        </button>
      </div>
      <div className={styles.chatsHeader}>
        <Avatar src={auth.photoUrl} />
        <h1>Chats</h1>
      </div>
    </div>
  );
};

export default ChatsHeader;
