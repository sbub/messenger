import React from 'react';

class ErrorBoundry extends React.Component {
  state = {
    hasError: false,
    errorInfo: null
  }
  componentDidCatch(error, errorInfo) {
    this.setState({
      hasError: true,
      errorInfo
    })
  }
  render() {
    if(this.state.hasError) {
      return (
        <p>An error while rendering occured</p>
      )
    }
    return this.props.children;
  }
}

export default ErrorBoundry;
