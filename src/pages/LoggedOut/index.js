import React from "react";
import Login from "components/Form/Login";
import styles from "./LoggedOut.module.css";

const LoggedOut = () => {
  return (
    <div className={styles.container}>
      <h1>Welcome to Messenger</h1>
      <Login />
    </div>
  );
};

export default LoggedOut;
