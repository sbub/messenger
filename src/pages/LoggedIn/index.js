import React from "react";
import styles from "./LoggedIn.module.css";
import Messages from "components/Messages";
import Header from "components/Header";
import ChatsHeader from "components/ChatsHeader";
import Conversation from "components/Conversation";
import MessageInput from "components/MessageInput";
import MessagesProvider from "modules/messages/context";
import InterlocutorProvider from "modules/interlocutor/context";

const ChatsSection = () => {
  return (
    <div className={styles.chatsSection}>
      <ChatsHeader />
      <Messages />
    </div>
  );
};

const MessageSection = () => {
  return (
    <div className={styles.messageSection}>
      <Header />
      <Conversation>
        <MessageInput />
      </Conversation>
    </div>
  );
};

const LoggedIn = () => {
  return (
    <div className={styles.container}>
      <MessagesProvider>
        <InterlocutorProvider>
          <ChatsSection />
          <MessageSection />
        </InterlocutorProvider>
      </MessagesProvider>
    </div>
  );
};

export default LoggedIn;
