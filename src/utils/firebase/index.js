import { auth, db } from "./firebase";

const usersRef = db.collection("users");

export const onAuthStateChanged = callback => {
  auth().onAuthStateChanged(callback);
};

export async function signup({ email, password, displayName, photoUrl }) {
  try {
    const { user } = await auth().createUserWithEmailAndPassword(
      email,
      password
    );
    await user.updateProfile({
      displayName,
      photoURL: photoUrl
    });
    await usersRef.add({
      uid: user.uid,
      displayName: displayName,
      photoUrl: photoUrl
    });
  } catch (e) {
    throw e;
  }
}

export async function login(email, password) {
  return await auth().signInWithEmailAndPassword(email, password);
}

export async function logout() {
  await auth().signOut();
}
