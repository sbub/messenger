import { db } from "utils/firebase/firebase";
const usersRef = db.collection("users");

const getData = async response => {
  const result = await response.docs.map(doc => ({
    id: doc.id,
    ...doc.data()
  }));
  return result;
};

export const loadOneUser = async (uids, authenticatedUserId) => {
  // in uids array select user id which is not a curently
  // logged in user
  const [userId] = uids.filter(id => id !== authenticatedUserId);
  try {
    const response = await usersRef.where("uid", "==", userId).get();
    const [result] = await getData(response);
    return result;
  } catch (err) {
    console.error("An error occured while loading a user ", userId, { err });
  }
};

export const loadAllUsers = async authenticatedUserId => {
  try {
    const response = await usersRef.get();
    const result = await response.docs
      .map(doc => ({
        id: doc.id,
        ...doc.data()
      }))
      .filter(u => u.uid !== authenticatedUserId);
    return result;
  } catch (err) {
    console.error("An error occured while loading all users ", { err });
  }
};
