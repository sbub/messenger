export type MessageProps = {
  id: string,
  lastMessage: {
    message: string,
    uid: string
  },
  uids: Array<string>,
  threadId: string,
  isRead: { [string]: boolean },
  isSelected: boolean
};
