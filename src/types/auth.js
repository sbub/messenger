export type AuthProps = {
  authAttempted: boolean,
  auth: {
    displayName: string,
    photoUrl: string,
    uid: string
  } | null
};
