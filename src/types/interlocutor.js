export type InterlocutorProps = {
  id: string,
  displayName?: string,
  photoUrl?: string,
  uid: string
};
