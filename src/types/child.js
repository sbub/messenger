import * as React from "react";

export type ChildProps = {
  children?: React.Node
};
