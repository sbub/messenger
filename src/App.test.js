import React from "react";
import { render } from "@testing-library/react";
import useAuth from "modules/auth/useAuth";
import { useAuthState } from "modules/auth/context";
import useMessages from "components/Messages/useMessages";
import useLoadInterlocutor from "components/Messages/useLoadInterlocutor";
import { App } from "./App";

jest.mock("modules/auth/useAuth");
jest.mock("modules/auth/context");
jest.mock("components/Messages/useMessages");
jest.mock("components/Messages/useMessages");
jest.mock("components/Messages/useLoadInterlocutor");

test("displays nothing when user hasn't attempted authentication", () => {
  useAuth.mockReturnValue({
    authAttempted: false,
    auth: null
  });
  const { container } = render(<App />);
  expect(container.firstChild).toBe(null);
});

test("displays LoggedOut component when user authentication failed", () => {
  useAuth.mockReturnValue({
    authAttempted: true,
    auth: null
  });
  const { getByText } = render(<App />);
  expect(getByText(/^Welcome to Messenger/)).toBeInTheDocument();
});

test("displays LoggedIn component when user authentication was successful", async () => {
  // const Auth = createContext();
  const authState = {
    authAttempted: true,
    auth: {
      uid: "Cb5AfSLVhmcalHVfaWgYb1I5IpW2",
      displayName: "user1"
    }
  };
  useAuth.mockReturnValue(authState);
  useAuthState.mockReturnValue(authState);
  useMessages.mockReturnValue({ loading: false, status: null });
  useLoadInterlocutor.mockReturnValue({ loading: false, users: [] });
  const { getByText } = render(<App />);
  expect(getByText("Chats")).toBeInTheDocument();
});
