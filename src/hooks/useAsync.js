import { useEffect } from "react";

const useAsync = (asyncFn, [...deps]) => {
  useEffect(() => {
    let isCurrent = true;
    if (isCurrent) {
      asyncFn();
    }
    return () => {
      isCurrent = false;
    };
  }, [asyncFn, deps]);
};

export default useAsync;
