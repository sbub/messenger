import React from "react";
import "./App.css";

import LoggedIn from "./pages/LoggedIn";
import LoggedOut from "./pages/LoggedOut";
import useAuth from "./modules/auth/useAuth";
import { AuthProvider } from "./modules/auth/context";
import ErrorBoundry from "components/ErrorBoundry";

export function App() {
  // start authentication process
  const { authAttempted, auth } = useAuth();
  if (!authAttempted) return null;
  return auth ? <LoggedIn /> : <LoggedOut />;
}

export default function() {
  return (
    <ErrorBoundry>
      <AuthProvider>
        <App />
      </AuthProvider>
    </ErrorBoundry>
  );
}
